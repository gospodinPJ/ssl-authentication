// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef SSLSERVER_H
#define SSLSERVER_H

#include <string>
#include <iostream>
#include <mutex>
#include <functional>
#include <unordered_set>

#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include <openssl/crypto.h>

#include "params.h"
#include "session.h"


class SSLServer
{
public:
    explicit SSLServer(boost::asio::io_service& io_service, const Params& parameters);
    SSLServer(const SSLServer& ) = delete;
    ~SSLServer();

    void init_server(const Params& parameters);
    void start_async_accept();

private:
    //bool verify_date_client_certificate_
    void handle_accept(Session *new_session, const boost::system::error_code& error);
    void close_session(Session *sess);

    std::string certificate_chain_file_;
    std::string private_key_file_;
    std::string tmp_dh_file_;

    boost::asio::io_service& io_service_;
    std::shared_ptr<boost::asio::ip::tcp::acceptor> acceptor_;
    std::shared_ptr<boost::asio::ssl::context> context_;

    std::unordered_set<Session * > sessions_;

    std::mutex mtx_;
};

#endif // SSLSERVER_H
