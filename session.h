#ifndef SESSION_H
#define SESSION_H

#include <cstdlib>
#include <vector>
#include <functional>

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include <openssl/crypto.h>

#include <easylogging++.h>

#define UNUSED(x) ((void)(x))


typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

class Session
{
public:
    Session(boost::asio::io_service& io_service, boost::asio::ssl::context& context, const std::function<void (Session *)>& del_callback);
    ~Session();

    void init();
    void read_data(const boost::system::error_code& error);
    void write_data(std::string &msg, const boost::system::error_code& error);
    ssl_socket::lowest_layer_type& get_socket() {
        return socket_.lowest_layer();
    }

private:
    boost::asio::io_service& io_service_;
    ssl_socket socket_;
    std::function<void (Session *)> async_close_this;
    std::vector<char> data_;
};

#endif // SESSION_H
