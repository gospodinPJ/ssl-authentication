// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <signal.h>

#include <boost/asio.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/bind.hpp>
#include <boost/program_options.hpp>

#include <easylogging++.h>

#include "sslserver.h"
#include "params.h"



INITIALIZE_EASYLOGGINGPP

//client part: http://www.boost.org/doc/libs/1_47_0/doc/html/boost_asio/example/ssl/client.cpp
const auto conf_file = "./server.conf";

int init_config(Params& parameters)
{
    int res = 0;

    std::ifstream settings_file(conf_file);
    boost::program_options::options_description desc;
    boost::program_options::variables_map vm;

    boost::program_options::options_description main_desc("basic options");
    main_desc.add_options()
            ("basic.port", boost::program_options::value<unsigned>(), "port");
    desc.add(main_desc);

    boost::program_options::options_description log_desc("logging options");
    log_desc.add_options()
            ("logging.log", boost::program_options::value<std::string>(), "log file");
    desc.add(log_desc);


    boost::program_options::options_description server_desc("server options");
    server_desc.add_options()
            ("server.certificate_chain_file", boost::program_options::value<std::string>(), "certificate file")
            ("server.private_key_file", boost::program_options::value<std::string>(), "private key")
            ("server.tmp_dh_file", boost::program_options::value<std::string>(), "temporary file")
            ("server.verify_file", boost::program_options::value<std::string>(), "verify file");
    desc.add(server_desc);


    try {
        boost::program_options::store(boost::program_options::parse_config_file(settings_file, desc), vm);
        boost::program_options::notify(vm);

        //[basic]
        parameters.port = vm["basic.port"].as<unsigned>();

        //[logging]
        parameters.log_file.assign(vm["logging.log"].as<std::string>());

        //[server]
        parameters.certificate_chain_file.assign(vm["server.certificate_chain_file"].as<std::string>());
        parameters.private_key_file.assign(vm["server.private_key_file"].as<std::string>());
        parameters.tmp_dh_file.assign(vm["server.tmp_dh_file"].as<std::string>());
        parameters.verify_file.assign(vm["server.verify_file"].as<std::string>());
    }
    catch (std::exception &e) {
        std::cerr << e.what() <<std::endl;
        res = -1;
    }
    settings_file.close();

    return res;
}


int main()
{

    Params parameters;
    if(init_config(parameters) == -1) {
        return -1;
    }

    el::Loggers::reconfigureAllLoggers(el::ConfigurationType::Format, "%level %datetime{%d-%M-%Y %H:%m:%s} %loc:\t%msg");
    el::Loggers::reconfigureAllLoggers(el::ConfigurationType::ToStandardOutput, "true");
    el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
    el::Loggers::reconfigureAllLoggers(el::ConfigurationType::Filename, parameters.log_file);

    boost::asio::io_service io_service;

    //Server
    try
    {
        std::shared_ptr<SSLServer> serv(new SSLServer(io_service, parameters));
        serv->init_server(parameters);

        //handler for stop server (ctrl-c)
        boost::asio::signal_set signs(io_service, SIGINT, SIGTERM);
        signs.async_wait([&serv, &io_service](const boost::system::error_code & err, int signal){
            UNUSED(err);
            UNUSED(signal);

            io_service.stop();
            serv.reset();
            exit(0);
        });

        serv->start_async_accept();
        io_service.run();
    }
    catch(std::exception& e)
    {
        LOG(ERROR) << e.what();
        io_service.stop();
        exit(-1);
    }

    return 0;
}
