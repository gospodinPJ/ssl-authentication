#include "sslserver.h"

SSLServer::SSLServer(boost::asio::io_service& io_service, const Params &parameters) :
    io_service_(io_service),
    context_(std::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context(io_service, boost::asio::ssl::context::sslv23)))
{
    context_->set_options( boost::asio::ssl::context::default_workarounds
                          | boost::asio::ssl::context::no_sslv2
                          | boost::asio::ssl::context::single_dh_use);

    acceptor_ = std::make_shared<boost::asio::ip::tcp::acceptor>
            (boost::asio::ip::tcp::acceptor(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), parameters.port)));
}

SSLServer::~SSLServer()
{
    for(auto& k: sessions_) {
        delete k;
    }
}


void SSLServer::init_server(const Params &parameters)
{
    context_->use_certificate_chain_file(parameters.certificate_chain_file);
    context_->use_private_key_file(parameters.private_key_file, boost::asio::ssl::context::pem);
    context_->use_tmp_dh_file(parameters.tmp_dh_file);
    context_->load_verify_file(parameters.verify_file);
    context_->set_verify_mode(boost::asio::ssl::context::verify_peer | boost::asio::ssl::context::verify_client_once);
    context_->set_verify_callback([](bool is_verified, boost::asio::ssl::verify_context& ctx) -> bool
    {
        UNUSED(is_verified);
        UNUSED(ctx);

        //X509 check...

        return true;
    });
}


void SSLServer::start_async_accept()
{
    Session *new_session = new Session(io_service_, *context_, [this](Session *sess) {
            close_session(sess);
    });

    std::unique_lock<std::mutex> ul(mtx_);
    try {
        sessions_.insert((new_session));
    }
    catch(std::exception& e) {
        LOG(WARNING) << e.what();
    }
    ul.unlock();

    acceptor_->async_accept(new_session->get_socket(), [this, new_session](const boost::system::error_code& error)
    {
        //we wait for the first connection
        handle_accept(new_session, error);
    });
}


void SSLServer::handle_accept(Session *new_session, const boost::system::error_code &error)
{
    if (!error) {
        new_session->init();

        //for new request
        Session *new_session = new Session(io_service_, *context_, [this](Session *sess) {
                close_session(sess);
        });

        std::unique_lock<std::mutex> ul(mtx_);
        try {
            sessions_.insert((new_session));
        }
        catch(std::exception& e) {
            LOG(WARNING) << e.what();
        }
        ul.unlock();

        acceptor_->async_accept(new_session->get_socket(), [this, new_session](const boost::system::error_code& error)
        {
            handle_accept(new_session, error);
        });
    }
    else {
        LOG(WARNING) << error.message();

        io_service_.post([this](){
            start_async_accept();
        });
    }
}


void SSLServer::close_session(Session *sess)
{
    LOG(DEBUG) << "close session";

    sessions_.erase(sess);
    delete sess;
    sess = nullptr;
}
