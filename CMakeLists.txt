project(SSL_Server)
cmake_minimum_required(VERSION 2.8)
set(CMAKE_CXX_FLAGS "-std=c++11")

#boost::system is needed to boost::asio
find_package( Boost COMPONENTS system thread program_options REQUIRED )
if(Boost_FOUND)
    set(boost_f "Boost was found: ")
    message(${boost_f} ${Boost_VERSION} "\n")
    include_directories( ${Boost_INCLUDE_DIRS} )
endif()

#OpenSSL library
find_package( OpenSSL )
if(OPENSSL_FOUND)
    set(openssl_f "OpenSSL was found: ")
    message(${openssl_f}  ${OPENSSL_VERSION} "\n")
else()
    message("OpenSSL wasn't found\n")
endif()

aux_source_directory(. SRC_LIST)
add_executable( ${PROJECT_NAME} ${SRC_LIST})

if( Boost_FOUND )
    TARGET_LINK_LIBRARIES( ${PROJECT_NAME} ${Boost_LIBRARIES} )
    TARGET_LINK_LIBRARIES( ${PROJECT_NAME} ${Boost_SYSTEM_LIBRARY} )
endif()

if( OPENSSL_FOUND )
    TARGET_LINK_LIBRARIES( ${PROJECT_NAME} ${OPENSSL_LIBRARIES} )
endif()
