#ifndef PARAMS_H
#define PARAMS_H

#include <string>

struct Params {

    //[basic]
    unsigned port;

    //[logging]
    std::string log_file;

    //[server]
    std::string certificate_chain_file;
    std::string private_key_file;
    std::string tmp_dh_file;
    std::string verify_file;
};


#endif // PARAMS_H

