#include "session.h"

Session::Session(boost::asio::io_service &io_service, boost::asio::ssl::context &context, const std::function<void(Session *)> &del_callback) :
    io_service_(io_service), socket_(io_service, context)
{
    async_close_this = del_callback;
    data_.resize(1024);
}

Session::~Session()
{
    ;
}

void Session::init()
{
    socket_.async_handshake(boost::asio::ssl::stream_base::server, [this](const boost::system::error_code& error)
    {
        LOG(INFO) << "Handshake with " << get_socket().remote_endpoint().address() << ":" <<  get_socket().remote_endpoint().port();

        if(!error || error.value() == ERR_PACK(ERR_LIB_SSL, 0, SSL_R_SHORT_READ)) {
            read_data(error);
        }
        else {
            LOG(WARNING) << error.message();
        }
    });
}


void Session::read_data(const boost::system::error_code &error)
{
    if (!error) {
        socket_.async_read_some(boost::asio::buffer(data_), [this](const boost::system::error_code& error, std::size_t sz)
        {
            std::string msg;
            for(size_t i=0; i<sz; ++i) {
                msg += data_[i];
            }

            LOG(INFO) << "Message from client: " << msg << " " << error ;

            if(msg == "q") {
                //close session
                io_service_.post([this]() {
                    async_close_this(this);
                });
            }
            else {
                write_data(msg, error);
            }
        });
    }
    else {
        LOG(WARNING) << error.message();
    }
}


void Session::write_data(std::string& msg, const boost::system::error_code &error)
{
    if(!error) {
        std::reverse(std::begin(msg), std::end(msg));
        boost::asio::async_write(socket_, boost::asio::buffer(msg), [this](const boost::system::error_code& error, std::size_t bytes)
        {
            UNUSED(error);
            UNUSED(bytes);
        });

        read_data(error);
    }
    else {
        LOG(WARNING) << error.message();
    }
}
